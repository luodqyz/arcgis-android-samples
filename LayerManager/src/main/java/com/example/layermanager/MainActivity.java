package com.example.layermanager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISDynamicMapServiceLayer;
import com.esri.android.map.ags.ArcGISLayerInfo;
import com.esri.android.map.ags.ArcGISTiledMapServiceLayer;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

public class MainActivity extends AppCompatActivity {

    private MapView mMapView;
    private ArcGISTiledMapServiceLayer mBaseLayer;
    private ArcGISDynamicMapServiceLayer mSpecialLayer;
    private AndroidTreeView tView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mMapView = (MapView) findViewById(R.id.mapView);
        mBaseLayer = new ArcGISTiledMapServiceLayer("http://map.geoq.cn/ArcGIS/rest/services/ChinaOnlineCommunity_Mobile/MapServer");
        mSpecialLayer = new ArcGISDynamicMapServiceLayer("http://map.geoq.cn/ArcGIS/rest/services/Thematic_Drawings/MapServer");
        mSpecialLayer.setName("专题图");
        mBaseLayer.setName("基础底图");
        mMapView.addLayer(mBaseLayer);
        mMapView.addLayer(mSpecialLayer);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                        .setIcon(R.drawable.i_layers)
                        .setTitle("图层管理").setNegativeButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mMapView.setScale(mMapView.getScale() + 100, true);//达到刷新效果

                            }
                        }).create();
                LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                View rootView = inflater.inflate(R.layout.main_selectable_nodes, null);
                TreeNode root = TreeNode.root();
                dialog.setView(rootView);
                ViewGroup containerView = (ViewGroup) rootView.findViewById(R.id.container);
                LayerManagerHelper.getInstance().createLayerControlTree(root, mMapView);
                tView = new AndroidTreeView(MainActivity.this, root);
                tView.setDefaultAnimation(true);
                tView.setSelectionModeEnabled(true);
                containerView.addView(tView.getView());
                dialog.show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

        }

        return super.onOptionsItemSelected(item);
    }

}
