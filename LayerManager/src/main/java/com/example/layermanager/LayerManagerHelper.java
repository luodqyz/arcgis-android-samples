package com.example.layermanager;

import android.content.Context;

import com.esri.android.map.Layer;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISDynamicMapServiceLayer;
import com.esri.android.map.ags.ArcGISLayerInfo;
import com.example.layermanager.tree.holder.IconTreeItemHolder;
import com.example.layermanager.tree.holder.SelectableHeaderHolder;
import com.example.layermanager.tree.holder.SelectableItemHolder;
import com.unnamed.b.atv.model.TreeNode;

/**
 * Created by Administrator on 2016/9/21.
 */

public class LayerManagerHelper {

    public static LayerManagerHelper instance;

    public static LayerManagerHelper getInstance() {
        if (instance == null) {
            return new LayerManagerHelper();
        }
        return instance;
    }

    public void createLayerControlTree(TreeNode root, MapView mapView) {
        Layer[] layers = mapView.getLayers();
        for (Layer layer : layers) {
            if (layer instanceof ArcGISDynamicMapServiceLayer) {
                ArcGISDynamicMapServiceLayer dynamicMapServiceLayer = (ArcGISDynamicMapServiceLayer) layer;
                IconTreeItemHolder.IconTreeItem parentItem = new IconTreeItemHolder.IconTreeItem(R.string.ic_earth, null);
                parentItem.text = layer.getName();
                TreeNode subNode = new TreeNode(parentItem).setViewHolder(new SelectableHeaderHolder(mapView.getContext()));
                subNode.setSelectable(layer.isVisible());
                root.addChild(subNode);
                if (dynamicMapServiceLayer.getLayers().length > 0) {
                    for (ArcGISLayerInfo layerInfo : dynamicMapServiceLayer.getLayers()) {
                        if (layerInfo.getLayers().length > 0) {
                            createLayerInfoTree(subNode, layerInfo, mapView);
                        } else {
                            TreeNode node2 = new TreeNode(layerInfo).setViewHolder(new SelectableItemHolder(mapView.getContext()));
                            subNode.addChild(node2);
                            node2.setSelected(layerInfo.isVisible());
                        }
                    }
                }
            }
        }
    }


    private void createLayerInfoTree(TreeNode folder, ArcGISLayerInfo layerInfo, MapView mapView) {
        TreeNode node = null;
        if (layerInfo.getLayers().length > 0) {
            IconTreeItemHolder.IconTreeItem item = new IconTreeItemHolder.IconTreeItem(R.string.ic_folder, layerInfo.getName());
            item.layerInfo = layerInfo;
            node = new TreeNode(item).setViewHolder(new SelectableHeaderHolder(mapView.getContext()));
            for (ArcGISLayerInfo layer : layerInfo.getLayers()) {
                createLayerInfoTree(node, layer, mapView);
            }
        } else {
            node = new TreeNode(layerInfo).setViewHolder(new SelectableItemHolder(mapView.getContext()));
            node.setSelected(layerInfo.isVisible());
        }
        folder.addChild(node);
    }
}
